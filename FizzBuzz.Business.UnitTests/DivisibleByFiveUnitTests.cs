﻿using System;
using FizzBuzz.Business.Interfaces;
using FizzBuzz.Business.Implementations;


using NUnit.Framework;
using Moq;


namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class DivisibleByFiveUnitTest
    {
        private IDivisible divisibleByFive;
        private Mock<ICheckForDay> mockDay;

        [SetUp]
        public void LoadContext()
        {
            mockDay = new Mock<ICheckForDay>();
            divisibleByFive = new DivisibleByFive(mockDay.Object);
        }

        [TestCase(5, ExpectedResult = true)]
        [TestCase(10, ExpectedResult = true)]
        [TestCase(15, ExpectedResult = true)]
        [TestCase(20, ExpectedResult = true)]
        [TestCase(25, ExpectedResult = true)]
        [TestCase(30, ExpectedResult = true)]
        [TestCase(35, ExpectedResult = true)]
        [TestCase(40, ExpectedResult = true)]
        [TestCase(45, ExpectedResult = true)]
        [TestCase(50, ExpectedResult = true)]
        public bool IsDivisible_Should_Return_True_If_Value_Is_Divisible_By_Five(int number)
        {
            //Act
            var result = divisibleByFive.IsDivisible(number);

            //Assert
            return result;
        }

        [TestCase(6, ExpectedResult = false)]
        [TestCase(11, ExpectedResult = false)]
        [TestCase(16, ExpectedResult = false)]
        [TestCase(21, ExpectedResult = false)]
        [TestCase(26, ExpectedResult = false)]
        [TestCase(31, ExpectedResult = false)]
        [TestCase(36, ExpectedResult = false)]
        [TestCase(41, ExpectedResult = false)]
        [TestCase(46, ExpectedResult = false)]
        [TestCase(51, ExpectedResult = false)]
        public bool IsDivisible_Should_Return_False_If_Value_Is_Divisible_By_Five(int number)
        {
            //Act
            var result = divisibleByFive.IsDivisible(number);

            //Assert
            return result;
        }

        [TestCase(5, ExpectedResult = "buzz")]
        [TestCase(10, ExpectedResult = "buzz")]
        [TestCase(15, ExpectedResult = "buzz")]
        [TestCase(20, ExpectedResult = "buzz")]
        [TestCase(25, ExpectedResult = "buzz")]
        [TestCase(30, ExpectedResult = "buzz")]
        [TestCase(35, ExpectedResult = "buzz")]
        [TestCase(40, ExpectedResult = "buzz")]
        [TestCase(45, ExpectedResult = "buzz")]
        [TestCase(50, ExpectedResult = "buzz")]
        public string GetMessage_Should_Return_Buzz_If_Divisible_By_Five(int number)
        {
            //Arrange
            mockDay.Setup(p => p.IsWednesday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleByFive.GetMessage(number);

            //Assert
            return result;
        }

        [TestCase(5, ExpectedResult = "wuzz")]
        [TestCase(10, ExpectedResult = "wuzz")]
        [TestCase(15, ExpectedResult = "wuzz")]
        [TestCase(20, ExpectedResult = "wuzz")]
        [TestCase(25, ExpectedResult = "wuzz")]
        [TestCase(30, ExpectedResult = "wuzz")]
        [TestCase(35, ExpectedResult = "wuzz")]
        [TestCase(40, ExpectedResult = "wuzz")]
        [TestCase(45, ExpectedResult = "wuzz")]
        [TestCase(50, ExpectedResult = "wuzz")]
        public string GetMessage_Should_Return_Wuzz_If_Divisible_By_Five_And_Today_Is_Wednesday(int number)
        {
            //Arrange
            mockDay.Setup(p => p.IsWednesday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleByFive.GetMessage(number);

            //Assert
            return result;
        }
    }
}
