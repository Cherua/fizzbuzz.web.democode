﻿using System;

using FizzBuzz.Business.Interfaces;
using FizzBuzz.Business.Implementations;

using NUnit.Framework;
using Moq;


namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class DivisibleByThreeUnitTest
    {
        private IDivisible divisibleByThree;
        private Mock<ICheckForDay> mockDay;

        [SetUp]
        public void LoadContext()
        {
            mockDay = new Mock<ICheckForDay>();
            divisibleByThree = new DivisibleByThree(mockDay.Object);
        }

        [TestCase(3, ExpectedResult = true)]
        [TestCase(6, ExpectedResult = true)]
        [TestCase(9, ExpectedResult = true)]
        [TestCase(12, ExpectedResult = true)]
        [TestCase(15, ExpectedResult = true)]
        [TestCase(18, ExpectedResult = true)]
        [TestCase(21, ExpectedResult = true)]
        [TestCase(24, ExpectedResult = true)]
        [TestCase(27, ExpectedResult = true)]
        [TestCase(30, ExpectedResult = true)]
        public bool IsDivisible_Should_Return_True_If_Value_Is_Divisible_By_Three(int number)
        {
            //Act
            var result = divisibleByThree.IsDivisible(number);

            //Assert
            return result;
        }

        [TestCase(1, ExpectedResult = false)]
        [TestCase(2, ExpectedResult = false)]
        [TestCase(4, ExpectedResult = false)]
        [TestCase(7, ExpectedResult = false)]
        [TestCase(8, ExpectedResult = false)]
        [TestCase(11, ExpectedResult = false)]
        [TestCase(13, ExpectedResult = false)]
        [TestCase(16, ExpectedResult = false)]
        [TestCase(17, ExpectedResult = false)]
        [TestCase(19, ExpectedResult = false)]
        public bool IsDivisible_Should_Return_False_If_Value_Is_Divisible_By_Three(int number)
        {
            //Act
            var result = divisibleByThree.IsDivisible(number);

            //Assert
            return result;
        }

        [TestCase(3, ExpectedResult = "fizz")]
        [TestCase(6, ExpectedResult = "fizz")]
        [TestCase(9, ExpectedResult = "fizz")]
        [TestCase(12, ExpectedResult = "fizz")]
        [TestCase(15, ExpectedResult = "fizz")]
        [TestCase(18, ExpectedResult = "fizz")]
        [TestCase(21, ExpectedResult = "fizz")]
        [TestCase(24, ExpectedResult = "fizz")]
        [TestCase(27, ExpectedResult = "fizz")]
        [TestCase(30, ExpectedResult = "fizz")]
        public string GetMessage_Should_Return_Fizz_If_Divisible_By_Three(int number)
        {
            //Arrange
            mockDay.Setup(p => p.IsWednesday(DateTime.Today.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleByThree.GetMessage(number);

            //Assert
            return result;
        }

        [TestCase(3, ExpectedResult = "wizz")]
        [TestCase(6, ExpectedResult = "wizz")]
        [TestCase(9, ExpectedResult = "wizz")]
        [TestCase(12, ExpectedResult = "wizz")]
        [TestCase(15, ExpectedResult = "wizz")]
        [TestCase(18, ExpectedResult = "wizz")]
        [TestCase(21, ExpectedResult = "wizz")]
        [TestCase(24, ExpectedResult = "wizz")]
        [TestCase(27, ExpectedResult = "wizz")]
        [TestCase(30, ExpectedResult = "wizz")]
        public string GetMessage_Should_Return_Wizz_If_Divisible_By_Three_And_Today_Is_Wednesday(int number)
        {
            //Arrange
            mockDay.Setup(p => p.IsWednesday(DateTime.Today.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleByThree.GetMessage(number);

            //Assert
            return result;
        }
    }
}

