﻿using System.Collections.Generic;

using FizzBuzz.Business.Interfaces;
using FizzBuzz.Business.Implementations;

using NUnit.Framework;
using Moq;

namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class FizzBuzzServiceUnitTest
    {
        private IFizzBuzzService fizzBuzzService;
        private Mock<IDivisible> mockDivisibleBy;

        List<string> finallist = new List<string>(new string[] {"1", "2", "fizz", "4", "buzz","fizz", "7", "8", "fizz", "buzz",
            "11", "fizz", "13", "14", "fizz buzz" });

        [SetUp]
        public void Loadcontext()
        {
            //Arrange
            mockDivisibleBy = new Mock<IDivisible>();

            mockDivisibleBy.Setup(p => p.IsDivisible(It.IsAny<int>())).Returns(true);
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 1))).Returns("1");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 2))).Returns("2");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 3))).Returns("fizz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 4))).Returns("4");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 5))).Returns("buzz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 6))).Returns("fizz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 7))).Returns("7");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 8))).Returns("8");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 9))).Returns("fizz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 10))).Returns("buzz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 11))).Returns("11");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 12))).Returns("fizz");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 13))).Returns("13");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 14))).Returns("14");
            mockDivisibleBy.Setup(p => p.GetMessage(It.Is<int>(i => i == 15))).Returns("fizz buzz");

            var listofinstances = new List<IDivisible> { mockDivisibleBy.Object };
            fizzBuzzService = new FizzBuzzService(listofinstances);
        }

        [TestCase(15)]
        public void GetFizzBuzzList_Should_Return_A_Fizz_Buzz_List(int number)
        {
            //Act
            var result = fizzBuzzService.GetFizzBuzzList(number);

            //Assert
            Assert.AreEqual(result, finallist);
            mockDivisibleBy.Verify(p => p.IsDivisible(It.IsAny<int>()), Times.Exactly(30));
        }
    }
}
