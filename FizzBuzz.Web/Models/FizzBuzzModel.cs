﻿using System.ComponentModel.DataAnnotations;
using PagedList;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Please enter a valid value")]
        [Range(1, 1000, ErrorMessage = "Please enter a number in the range 1 to 1000")]
        public int UserInput { get; set; }

        public IPagedList<string> ResultList { get; set; } 
    }
}