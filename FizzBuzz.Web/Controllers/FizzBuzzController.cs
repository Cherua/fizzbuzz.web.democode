﻿using System.Web.Mvc;
using FizzBuzz.Web.Models;

using FizzBuzz.Business.Interfaces;
using PagedList;

namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzBuzzService;
        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        // GET: FizzBuzz
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzModel());
        }
        
        [HttpGet]
        public ActionResult FizzBuzzList(FizzBuzzModel fizzBuzzModel, int? page)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", fizzBuzzModel);
            }
            var finaloutput = this.fizzBuzzService.GetFizzBuzzList(fizzBuzzModel.UserInput);
            fizzBuzzModel.ResultList = finaloutput.ToPagedList(page ?? 1, 20);
            return View("Index", fizzBuzzModel);
        }
    }
}