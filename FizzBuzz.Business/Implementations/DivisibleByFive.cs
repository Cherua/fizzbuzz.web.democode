﻿using System;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business.Implementations
{
    public class DivisibleByFive : IDivisible
    {
        private readonly ICheckForDay dayToday;
        public DivisibleByFive(ICheckForDay dayToday)
        {
            this.dayToday = dayToday;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }
        public string GetMessage(int number)
        {
            return (this.dayToday.IsWednesday(DateTime.Today.DayOfWeek)) == true ? "wuzz" : "buzz";
        }
    }
}
