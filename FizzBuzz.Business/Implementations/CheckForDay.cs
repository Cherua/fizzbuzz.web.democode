﻿using System;
using FizzBuzz.Business.Interfaces;

namespace FizzBuzz.Business.Implementations
{
    public class CheckForDay : ICheckForDay
    {
        public bool IsWednesday(DayOfWeek day)
        {
            return day == DayOfWeek.Wednesday;
        }
    }
}
